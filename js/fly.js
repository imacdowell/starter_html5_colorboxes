
/* fly weight pattern - from jsperf
 * use in place of $() in loops where you get a node rather than a jq object
 *
 *  use :
 * 	a.each(function() {
 * 		$.fly(this);
 * 	});
 *
 *
 */

(function($) {

	var fly = $(),
	push = Array.prototype.push;
	
	$.fly = function(elem) {
	
		var len = fly.length, i;
		
		if ($.isArray(elem)) {
			fly.length = 0;
			i = push.apply(fly, elem);
		} else {
			if (elem instanceof $) {
				return elem;
			}
			if (typeof elem == "string") {
				throw "use jQuery()";
			}
			fly[0] = elem;
			fly.length = i = 1;
		}
		
		// remove orphaned references
		while (i < len) {
			delete fly[i++];
		}
	
		return fly;
	};
	
})(jQuery);